# A project demonstrating Sequelize functionality

## What is Sequelize

[Sequelize](https://sequelize.org/master/index.html) is an open source Object Relational Mapping library.

###
- It minimizes the need to write sql statement specific to a database (although it allows this)
- It provides DB technology agnostic independence.  You can easily switch DBs (for instance between unit tests and deployment)
- It creates the schema 
- It supports application schema migration so that a deployment of a version upgrades the db schema
- It allows us to have apps that dont really care what the customer preference is for a DB (postgress, mysql,sql server)
- It manages DB connections for us
- It facilitates transactions (atomic operations across multiple DB tables)
- Works with primary and foreign key constraints and relationships

###
- It only works with relational DBs
- It is an analogue of Microsoft Entity Framework 
- It is type 4 driver all javascript approach, doesnt support windows authentication.  Tedious is the open source library for Sql Server integration

###
- It has been around for a long time (in the scale of all things javascript) and it now at version 6
- It is well documented and there are plently of question/answers available on the web.

### What does the code look like

```
  let model = await ModelFactory(/*db configuration*/)

  await model.Phase.upsert({
      id: '123',
      phaseName: 'Phase1'
  })

  let phase = await model.Phase.get('123')
```

## Mentions

- Dependencies
```
  "dependencies": {
    "sequelize": "^6.3.5", //The sequelize contract
    "sqlite3": "^5.0.0" //A driver for a database
    "pg": "^7.17.0", //postgress
    "tedious": "^4.1.0", //mssql
  }
```

- Creation of Model object
```
    const Phase = sequelize.define(
        'Phase',
        {
            id: {
                type: STRING,
                primaryKey: true
            },
            phaseName: {
                type: STRING
            },
            projectId: {
                type: STRING
            }
        },
        {
            schema: config.schema,
            tableName: 'Phase'
            // timestamps: false
        }
    )
```

- Unit Test with SQL Lite DB which can excercise all persist options without any infrastructure requirement

- The Model objects have additional meta data
```
let user = await model.User.get('123')
console.log(user)

User {
  dataValues: {
    id: '456',
    active: 'true',
    createdAt: 2020-09-15T11:08:57.631Z,
    updatedAt: 2020-09-15T11:08:57.631Z
  },
  _previousDataValues: {
    id: '456',
    active: 'true',
    createdAt: 2020-09-15T11:08:57.631Z,
    updatedAt: 2020-09-15T11:08:57.631Z
  },
  _changed: Set {},
  _options: {
    isNewRecord: false,
    _schema: null,
    _schemaDelimiter: '',
    raw: true,
    attributes: [ 'id', 'active', 'createdAt', 'updatedAt' ]
  },
  isNewRecord: false
}
```