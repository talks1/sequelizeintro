import Debug from 'debug'
import PhaseFactory from './model/Phase'
import UserFactory from './model/User'
import ProjectMemberFactory from './model/ProjectMember'

const debug = Debug('lib')

export async function ModelFactory(config) {
    const Sequelize = require('sequelize')

    const sequelize = new Sequelize(config.DB, config.USER, config.PASSWORD, {
        host: config.DBHOST,
        dialect: config.DBDIALECT,
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        },
        dialectOptions: {
            encrypt: true
        },
        //logging: console.log, //logging on
        logging: false, //logging off

        // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
        // operatorsAliases: false
        // timezone: 'utc'
    })

    let Phase = await PhaseFactory(sequelize, config)
    let User = await UserFactory(sequelize, config)
    let ProjectMember = await ProjectMemberFactory(sequelize, config)


    Phase.entity.belongsToMany(User.entity, { through: ProjectMember.entity });
    User.entity.belongsToMany(Phase.entity, { through: ProjectMember.entity });
    
    function close() {
        sequelize.close()
    }
    return {
        Phase,
        User,
        ProjectMember,
        close
    }
}
