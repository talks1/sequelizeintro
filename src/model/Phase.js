import Debug from 'debug'
import { Sequelize as sqlize, STRING, INTEGER, FLOAT, JSON } from 'sequelize'

const debug = Debug('lib')

export default async (sequelize, config) => {

    const Phase = sequelize.define(
        'Phase',
        {
            id: {
                type: STRING,
                primaryKey: true
            },
            phaseName: {
                type: STRING
            },
            projectId: {
                type: STRING
            }
        },
        {
            schema: config.schema,
            tableName: 'Phase'
            // timestamps: false
        }
    )

    if (config.RESYNC === 'true') {
        debug(`Syncing Phase ${config.RESYNC}`)
        await Phase.sync({ force: config.force })
    }


    async function upsert(record, site, tzoffset) {

        if (record.id !== undefined) {
            debug('Creating Phase')
            return await sequelize.models.Phase.create(record)
        } else {
            debug('Updating Phase')
            try {
                record= await sequelize.models.Phase.upsert(record, { returning: true })
            } catch (err) {
                console.log(err)
                return null
            }
            return record
        }
    }

    async function get(id = '') {
        let where = { id }
        return sequelize.models.Phase.findOne({ where })
    }

    async function query(siteId) {
        let where = {}
        if (siteId !== undefined) {
            where.siteId = siteId
        }
        return sequelize.models.Phase.findAll({ where })
    }

    async function destroy(id = '') {
        return Phase.destroy({ where: { id } })
    }

    return {
        entity: Phase,
        upsert,
        get,
        query,
        destroy
    }
}
