import Debug from 'debug'
import { Sequelize as sqlize, STRING, INTEGER, FLOAT, JSON } from 'sequelize'

const debug = Debug('lib')

export default async (sequelize, config) => {

    const ProjectMember = sequelize.define(
        'ProjectMember',
        {
            id: {
                type: STRING,
                primaryKey: true
            },
            phaseId: {
                type: STRING,
            },
            userId: {
                type: STRING
            },
            userRole: {
                type: STRING
            }
        },
        {
            schema: config.schema,
            tableName: 'ProjectMember'
            // timestamps: false
        }
    )

    if (config.RESYNC === 'true') {
        debug(`Syncing ProjectMember ${config.RESYNC}`)
        await ProjectMember.sync({ force: config.force })
    }

    async function upsert(record, site, tzoffset) {

        if (record.id !== undefined) {
            debug('Creating ProjectMember')
            return await sequelize.models.ProjectMember.create(record)
        } else {
            debug('Updating ProjectMember')
            try {
                record= await sequelize.models.ProjectMember.upsert(record, { returning: true })
            } catch (err) {
                console.log(err)
                return null
            }
            return record
        }
    }

    async function get(id = '') {
        let where = { id }
        return sequelize.models.ProjectMember.findOne({ where })
    }

    async function query(siteId) {
        let where = {}
        if (siteId !== undefined) {
            where.siteId = siteId
        }
        return sequelize.models.ProjectMember.findAll({ where })
    }

    async function destroy(id = '') {
        return ProjectMember.destroy({ where: { id } })
    }

    return {
        entity: ProjectMember,
        upsert,
        get,
        query,
        destroy
    }
}
