import Debug from 'debug'
import { Sequelize as sqlize, STRING, INTEGER, FLOAT, JSON } from 'sequelize'

const debug = Debug('lib')

export default async (sequelize, config) => {

    const User = sequelize.define(
        'User',
        {
            id: {
                type: STRING,
                primaryKey: true
            },
            active: {
                type: STRING
            }
        },
        {
            schema: config.schema,
            tableName: 'User'
            // timestamps: false
        }
    )

    if (config.RESYNC === 'true') {
        debug(`Syncing User ${config.RESYNC}`)
        await User.sync({ force: config.force })
    }

    async function upsert(record, site, tzoffset) {

        if (record.id !== undefined) {
            debug('Creating User')
            return await sequelize.models.User.create(record)
        } else {
            debug('Updating User')
            try {
                record= await sequelize.models.User.upsert(record, { returning: true })
            } catch (err) {
                console.log(err)
                return null
            }
            return record
        }
    }

    async function get(id = '') {
        let where = { id }
        return sequelize.models.User.findOne({ where })
    }

    async function query(siteId) {
        let where = {}
        if (siteId !== undefined) {
            where.siteId = siteId
        }
        return sequelize.models.User.findAll({ where })
    }

    async function destroy(id = '') {
        return User.destroy({ where: { id } })
    }

    return {
        entity: User,
        upsert,
        get,
        query,
        destroy
    }
}
