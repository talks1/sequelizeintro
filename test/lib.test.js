import Debug from 'debug'
import { describe } from 'riteway'

import {ModelFactory } from '../src'

import { unittest } from './unittest-config'

const debug = Debug('test')

let createdAtString = '2020-09-09 12:00:00'
let createdAt = new Date(createdAtString)

describe('Phase', async (assert) => {
  try {
      let model = await ModelFactory(unittest.database)

      await model.Phase.upsert({
          id: '123',
          phaseName: 'Phase1'
      })

      let phase = await model.Phase.get('123')
    
      assert({
        given: 'a phase',
        should: 'get return correct entity',
        actual: `${phase.id} ${phase.phaseName}`,
        expected: '123 Phase1'
      })

      await model.User.upsert({
        id: '456',
        active: 'true'
      })

      let user = await model.User.get('456')

      assert({
        given: 'a user',
        should: 'get return correct entity',
        actual: `${user.id} ${user.active}`,
        expected: '456 true'
      })

      await model.ProjectMember.upsert({
        id: '101',
        phaseId: '123'
      })

      let projectMember = await model.ProjectMember.get('101')

      assert({
        given: 'a project member',
        should: 'get return correct entity',
        actual: `${projectMember.id}`,
        expected: '101'
      })

  }catch(ex){
    console.log(ex)
}

})
