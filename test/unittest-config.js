let unittestConfig = {
    database: {
        DB: 'groundgauge',
        DBDIALECT: 'sqlite',
        USER: 'test',
        PASSWORD: 'password',
        force: true,
        RESYNC: 'true'
    },
  }

  let localConfig = {
    database: {
        DBHOST: 'localhost',
        DB: 'aureconalign',
        DBDIALECT: 'mssql',
        USER: 'aal',
        PASSWORD: 'PA22word',
        force: true,
        RESYNC: 'true'
    },
  }

  let cloudConfig = {
    database: {
      DBHOST: 'database-1.ap-southeast-2.rds.amazonaws.com',
      DBDIALECT: 'postgres',
      DB: 'groundgauge',
      USER: 'postgres',
      PASSWORD: process.env.POSTGRES_PASSWORD,
      schema: 'groundgaugedev',
      force: true,
      RESYNC: false
    },
    authentication: {
      enabled: false
    }
  }

  export const unittest = localConfig
